﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApplicationKatarzynaBialas.Models
{
    public class Client: DbContext
    {
        public int ID { get; set; }
        public int OrderID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Address { get; set; }
        public int Mobile { get; set; }
    }
}