﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApplicationKatarzynaBialas.Models
{
    public class Order: DbContext
    {
        public int OrderID { get; set; }
        public string PizzaName { get; set; }
        public string Ingredients { get; set; }
        public int Price { get; set; }
        public int Amount { get; set; }
    }
}