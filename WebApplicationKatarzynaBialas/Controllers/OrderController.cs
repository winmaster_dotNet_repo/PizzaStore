﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplicationKatarzynaBialas.Models;

namespace WebApplicationKatarzynaBialas.Controllers
{
    public class OrderController : Controller
    {
        // GET: Pizza
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            List<Pizza> pizzas;

            using (var ctx = new EFDbContext())
            {
                pizzas = ctx.Pizzas.ToList();
            }
            return View(pizzas);
        }

        [HttpPost]
        public ActionResult List(Order order)
        {
            using (var ctx = new EFDbContext())
            {
                ctx.Orders.Add(order);
                ctx.SaveChanges();
            }
            return View();
        }

        public ActionResult Basket()
        {
            return View(new Client());
        }

        [HttpPost]
        public ActionResult Basket(Client client)
        {
            if (!ModelState.IsValid)
            {
                return View(new Client());
            }
            using (var ctx = new EFDbContext())
            {
                ctx.Clients.Add(client);
                ctx.SaveChanges();
            }

            return RedirectToAction("Summary");
        }

        public ActionResult Summary()
        {
            return View();
        }

        public ActionResult CheckOut()
        {
            return View();
        }

        public ActionResult Thanks()
        {
            return View();
        }
    }
}