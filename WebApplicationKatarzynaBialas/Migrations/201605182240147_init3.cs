namespace WebApplicationKatarzynaBialas.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Clients", "Mobile", c => c.Int(nullable: false));
            AlterColumn("dbo.Clients", "Name", c => c.String());
            AlterColumn("dbo.Clients", "Surname", c => c.String());
            AlterColumn("dbo.Clients", "Address", c => c.String());
            DropColumn("dbo.Clients", "Contact");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Clients", "Contact", c => c.Int(nullable: false));
            AlterColumn("dbo.Clients", "Address", c => c.Int(nullable: false));
            AlterColumn("dbo.Clients", "Surname", c => c.Int(nullable: false));
            AlterColumn("dbo.Clients", "Name", c => c.Int(nullable: false));
            DropColumn("dbo.Clients", "Mobile");
        }
    }
}
