namespace WebApplicationKatarzynaBialas.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init5 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        OrderID = c.Int(nullable: false, identity: true),
                        PizzaName = c.String(),
                        Ingredients = c.String(),
                        Price = c.Int(nullable: false),
                        Amount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrderID);
            
            AddColumn("dbo.Clients", "OrderID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Clients", "OrderID");
            DropTable("dbo.Orders");
        }
    }
}
